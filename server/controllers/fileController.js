if (!process.env.NODE_ENV) require('dotenv').config()

exports.create_file = function(req, res) {

    let imageURL = req.protocol + '://' + req.headers.host + '/' + process.env.IMG_DIR_NAME + '/' + req.file.customname;

    res.json( { imageURL: imageURL } );

};