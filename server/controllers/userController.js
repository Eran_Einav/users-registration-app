const { validationResult } = require('express-validator');
const db = require('../services/databaseService')

exports.create_user = function(req, res) {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {

    return res.send({ errors: errors.array() });

  }

  let strippeTags = function(val){

    return val.replace(/(<([^>]+)>)/ig,"");

  },
  getCurrentDate = function(sep = '-'){

      let date = new Date(),
      fullYear = date.getFullYear(),
      month = date.getMonth() + 1,
      monthDay = date.getDate();

      if (month < 10) month = '0' + month;
      if (monthDay < 10) monthDay = '0' + monthDay;

      return fullYear + sep + month + sep + monthDay;
      
  },
  user = {
    user_first_name: strippeTags(req.body.firstName),
    user_last_name: strippeTags(req.body.lastName),
    user_gender: strippeTags(req.body.gender),
    user_pic_url: strippeTags(req.body.picURL),
    user_date: getCurrentDate()
  };

  db.createUser(user, function(data){

    res.send(data);

  });

};