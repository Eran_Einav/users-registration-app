
if (!process.env.NODE_ENV) require('dotenv').config()

const helmet = require('helmet')
const multer = require('multer')
const { body } = require('express-validator');
const express = require('express')
const app = express()
const AllowOriginURL = process.env.ALLOW_ORIGIN_URL || '*'
const fileDestDir = process.env.FILE_DEST_DIR || process.env.TMP || '/tmp/'
const storage = multer.diskStorage({

    destination: fileDestDir,
    filename: function (req, file, cb) {

        let originalnameArr = file.originalname.split(/\.(?=[^\.]+$)/);

        file.customname = originalnameArr[0] + '-' + +new Date + '.' + originalnameArr[1];

        cb(null, file.customname)

    }

})
const upload = multer({ storage: storage })
const port = 3000
const APIBase = '/api/v1/'
const validationArr = [
  body('lastName').isAlpha().isLength({ min: 2 }),
  body('firstName').isAlpha().isLength({ min: 2 }),
  body('gender').isAlpha().trim().isLength({ min: 4 }),
  body('picURL').custom(value => {

    if (value === '') return true;

    return /^(ftp|http|https):\/\/[^ "]+$/.test(value);
     
  })
]
const file_controller = require('./controllers/fileController')
const user_controller = require('./controllers/userController')

app.use(helmet())

app.use(express.json())

app.use('/' + process.env.IMG_DIR_NAME, express.static(fileDestDir))

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", AllowOriginURL);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(port, () => {

    console.log(`Listening on port ${port}!`)

})

app.post(APIBase + 'files', upload.single('picURL'), file_controller.create_file)

app.post(APIBase + 'users', validationArr, user_controller.create_user)