const mysql = require('mysql')
const databaseService = {

    connect(){

        const conCred = {
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            database: process.env.DB_NAME,
            port: process.env.DB_PORT
        }
        if (process.env.DB_SOCKET_PATH) conCred.socketPath = process.env.DB_SOCKET_PATH;
        
        const con = mysql.createConnection(conCred)

        con.connect(function(err) {

            if (err) throw err;

        })

        return con;

    },

    createUser(user, cb) {

      let con = this.connect(),
      sql = 'INSERT INTO users SET ?';

      con.query(sql, user, function (err, result) {

        if (err){

            cb({status: 'failure', content: err});

        }
        else {

            cb({status: 'success', content: "1 user record inserted, ID: " + result.insertId});

        }

      })

    }

}

module.exports = databaseService;