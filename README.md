# User Registration Application

User registration application based on Angular, Node.js & Express, MySQL.

## Getting Started

Open a terminal and navigate to where you want to clone the repository to, then run the following commands:

```
git clone https://Eran_Einav@bitbucket.org/Eran_Einav/users-registration-app.git
cd users-registration-app
```
The repository will be cloned into a new directory with name of "users-registration-app".
Then we are using `cd` command for changing the current working directory into the new "users-registration-app" directory, for the next steps.

### Prerequisites

1. [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en).
2. [Node.js and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

### Database migration

First, we need to create database for the project and migrate the database tables: i want to add auto migration for the database and the database tables but for now just import the attached "usersreg.sql" file in the "database" directory into your database, it will create a new database with the name of "usersreg" and migrate the database tables.

### Installing and starting servers

In the terminal, Assuming you are already in the root of the project, run the following command:

```
npm run all:install-and-start
```
It should:

1. Install all the dependencies of the project.
2. Create server/.env file from the server/.env.example file content : server/.env file should include your database credentials for MySQL integration so modify the values of DB_* keys according to your database credentials.
2. Start Express server.
3. Start Angular development server.

## Authors

*Eran Einav*

## License

This project is licensed under the [MIT](https://choosealicense.com/licenses/mit/) License.