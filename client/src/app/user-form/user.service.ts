import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { User } from './user';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    API_URL = environment.API_Base_URL + 'users';

    constructor(private http: HttpClient) { }

    addUser(user: User): Observable<User> {

        return this.http.post<User>(this.API_URL, user);

    }

}