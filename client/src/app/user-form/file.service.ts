import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

    API_URL = environment.API_Base_URL + 'files';

    constructor(private http: HttpClient) { }

    addFile(fileToUpload: File): Observable<any> {

        const formData: FormData = new FormData();
        
        formData.append('picURL', fileToUpload, fileToUpload.name);

        return this.http.post<any>(this.API_URL, formData);

    }

}