import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators }   from '@angular/forms';

import { FileService } from './file.service';
import { UserService } from './user.service';

import { User } from './user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  fileToUpload: File = null;
  imageURL: any = '';
  genders = [
    'male',
    'female'
  ];
  userForm = new FormGroup({
    firstName: new FormControl('', [Validators.required, Validators.minLength(2)]),
    lastName: new FormControl('', [Validators.required, Validators.minLength(2)]),
    gender: new FormControl(this.genders[0]),
    picURL: new FormControl('')
  });
  signupSuccess: boolean = false;
  statusMessage: string = '';

  constructor(private fileService: FileService, private userService: UserService) { }

  ngOnInit() {
  }

  addFile(files: FileList): void {

    this.fileToUpload = files.item(0);

    this.fileService.addFile(this.fileToUpload).subscribe(res => this.imageURL = res.imageURL);

  }

  addUser(User: User): void {

    this.userService.addUser(User).subscribe(res => {

      if (res['status'] === 'success'){

        this.toggleSignupSuccess();

      }
      else {

        for (let i in res['errors']) {

          let errObj = res['errors'][i],
          field = this.userForm.get(errObj.param);

          field.setErrors({error: errObj.msg});

        }

      }

    });

  }

  onSubmit() {
    
    let form = this.userForm.controls,
    user = {
      firstName: form['firstName'].value,
      lastName: form['lastName'].value,
      gender: form['gender'].value,
      picURL: this.imageURL
    };

    this.addUser(user);

  }

  clearForm(){

    this.userForm.reset({
      firstName: '',
      lastName: '',
      gender: this.genders[0],
      picURL: ''
    });

    this.imageURL = '';

  }

  toggleSignupSuccess(){

    this.signupSuccess = !this.signupSuccess;

  }

  signupAgain(){

    this.clearForm();
    this.toggleSignupSuccess();

  }

  getErrorMessage(field: FormControl){

    let message = 'Invalid value';

    for (let i in field.errors){

      switch (i) {

        case 'minlength':
          // DOTO: Message for min length
          break;

        case 'required':
          message = 'Field is required!';
          break;

      }

      return message;

    }

  }

  validateField(field: FormControl){

    if (field.errors){

      let message = this.getErrorMessage(field);
      field.setErrors({error: message});

    }

  }

  blur(field: FormControl){

    let strippeTagsValue = field.value.replace(/(<([^>]+)>)/ig,"");
    field.setValue(strippeTagsValue);
    this.validateField(field);

  }

  input(field: FormControl){

    this.validateField(field);
    
  }

}
