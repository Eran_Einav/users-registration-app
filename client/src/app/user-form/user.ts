export class User {
  firstName: string;
  lastName: string;  
  gender: string;
  picURL: string;
}